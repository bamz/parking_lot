package com.gojek.parkinglot.util

object RichString {

    sealed case class RichString(str: String) {

        def toCamelCase(): String = {
            val split = str.split("_")
            val tail = split.tail.map { x => x.head.toUpper + x.tail }
            split.head + tail.mkString
        }

    }

    implicit def stringToRichString(str:String): RichString = RichString(str)

}
