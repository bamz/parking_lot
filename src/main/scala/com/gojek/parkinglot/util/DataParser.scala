package com.gojek.parkinglot.util

import java.io.{BufferedReader, File, FileNotFoundException, FileReader, IOException}
import com.gojek.parkinglot.command.InputMap
import com.gojek.parkinglot.model.ParkingLotModel


class DataParser {

    private val inputMap = new InputMap()
    private val parkingLot = new ParkingLotModel()

    private def getMethod(str: String, args: String*): Unit = {
        val method = inputMap.parameterMap.get(str)

        try {
            if (method != null) {
                args.length match {
                    case 0 => method.invoke(parkingLot)
                    case 1 => method.invoke(parkingLot, args.head)
                    case 2 => method.invoke(parkingLot, args.head, args(1))
                    case _ => println("Invalid Input")
                }
            } else {
                println("Invalid Input")
            }
        } catch {
            case e: Exception =>
                println(e.getMessage)

                method.getName match {
                    case "createParkingLot" => println("create_parking_lot [NUMBER], eg: create_parking_lot 6")
                    case "park" => println("park [REG_NUMBER] [COLOR], eg: park KA-01-HH-1234 White")
                    case "leave" => println("leave [SLOT_NUMBER], eg: leave 1")
                    case "status" => println("No parameter, type status only")
                    case "registrationNumbersForCarsWithColor" => println("registration_numbers_for_cars_with_color [COLOR], eg: registration_numbers_for_cars_with_color White")
                    case "slotNumbersForCarsWithColor" => println("slot_numbers_for_cars_with_color [COLOR], eg: slot_numbers_for_cars_with_color White")
                    case "slotNumberForRegistrationNumber" => println("slot_number_for_registration_number [REG_NUMBER], eg: slot_number_for_registration_number KA-01-HH-1234")
                    case x => println(s"Method $x not found")
                }
        }
    }

    def parseText(str: String): Unit = {
        val textStr = str.split(" ")
        textStr.length match {
            case 1 => getMethod(str)
            case 2 => getMethod(textStr.head, textStr(1))
            case 3 => getMethod(textStr.head, textStr(1), textStr(2))
            case _ => println("Invalid Input")
        }
    }

    def parseFile(fp: String): Unit = {
        val inputFile = new File(fp)
        try {
            val buff = new BufferedReader(new FileReader(inputFile))

            try {
                while (buff.ready()) {
                    parseText(buff.readLine().trim)
                }
            } catch {
                case e: IOException =>
                    println(e.getMessage)
                    println("Error in reading the input file")
            }
        } catch {
            case e: FileNotFoundException =>
                println(e.getMessage)
                println("File not found")
        }
    }

}
