package com.gojek.parkinglot.command

import java.util.HashMap
import java.lang.reflect.Method
import com.gojek.parkinglot.model.ParkingLotModel
import com.gojek.parkinglot.util.RichString.stringToRichString


class InputMap {

    val parameterMap = new HashMap[String, Method]()

    try {
        init()
    } catch {
        case e: NoSuchMethodException =>
            e.printStackTrace()
    }

    private def getParkingLotMethod(method: String) = {
        method match {
            case InputLabel.PARK => classOf[ParkingLotModel].getMethod(method.toCamelCase(), classOf[String], classOf[String])
            case InputLabel.STATUS => classOf[ParkingLotModel].getMethod(method.toCamelCase())
            case _ => classOf[ParkingLotModel].getMethod(method.toCamelCase(), classOf[String])
        }
    }

    private def init(): Unit = {
        parameterMap.put(InputLabel.CREATE_PARKING_LOT, getParkingLotMethod(InputLabel.CREATE_PARKING_LOT))
        parameterMap.put(InputLabel.PARK, getParkingLotMethod(InputLabel.PARK))
        parameterMap.put(InputLabel.LEAVE, getParkingLotMethod(InputLabel.LEAVE))
        parameterMap.put(InputLabel.STATUS, getParkingLotMethod(InputLabel.STATUS))
        parameterMap.put(InputLabel.REG_NUMBERS_FOR_CARS_WITH_COLOR, getParkingLotMethod(InputLabel.REG_NUMBERS_FOR_CARS_WITH_COLOR))
        parameterMap.put(InputLabel.SLOT_NUMBERS_FOR_CARS_WITH_COLOR, getParkingLotMethod(InputLabel.SLOT_NUMBERS_FOR_CARS_WITH_COLOR))
        parameterMap.put(InputLabel.SLOT_NUMBER_FOR_REG_NUMBER, getParkingLotMethod(InputLabel.SLOT_NUMBER_FOR_REG_NUMBER))
    }

}


object InputLabel {
    val CREATE_PARKING_LOT = "create_parking_lot"
    val PARK = "park"
    val LEAVE = "leave"
    val STATUS = "status"
    val REG_NUMBERS_FOR_CARS_WITH_COLOR = "registration_numbers_for_cars_with_color"
    val SLOT_NUMBERS_FOR_CARS_WITH_COLOR = "slot_numbers_for_cars_with_color"
    val SLOT_NUMBER_FOR_REG_NUMBER = "slot_number_for_registration_number"
}
