package com.gojek.parkinglot

import java.io.{BufferedReader, InputStreamReader}
import com.gojek.parkinglot.util.DataParser


object Main {

    def main(args: Array[String]): Unit = {

        val dataParser = new DataParser()
        args.length match {
            case 0 =>
                println("Please enter 'exit' to quit")
                println("Waiting for input...")

                while (true) {
                    try {
                        val buff = new BufferedReader(new InputStreamReader(System.in))
                        val inputStr = buff.readLine()

                        inputStr match {
                            case "exit" =>
                                println("Bye!")
                                System.exit(0)
                            case x => dataParser.parseText(x.trim())
                        }
                    } catch {
                        case e: Exception =>
                            e.printStackTrace()
                    }
                }

            case 1 => dataParser.parseFile(args.head)
            case _ => println("Invalid input.")
        }

    }

}
