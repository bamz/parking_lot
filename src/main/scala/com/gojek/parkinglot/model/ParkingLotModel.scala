package com.gojek.parkinglot.model

import java.util._
import com.gojek.parkinglot.dao.ParkingLotDao


class ParkingLotModel extends ParkingLotDao {

    /**
     * Number of parking lot
     */
    var numberOfParkingLot = 0

    val availableSlots: ArrayList[Int] = new ArrayList[Int]()

    private val mapCar: HashMap[String, Car] = new HashMap[String, Car]()
    private val mapRegColor: HashMap[String, String] = new HashMap[String, String]()
    private val mapRegsColor: HashMap[String, ArrayList[String]] = new HashMap[String, ArrayList[String]]()

    /**
     * Create parking lot
     *
     * @param lotCount - parking lot count
     */
    override def createParkingLot(lotCount: String): Unit = {
        try {
            numberOfParkingLot = lotCount.toInt
        } catch {
            case e: NumberFormatException =>
                println(e.getMessage)
                println("Invalid lot count data")
                numberOfParkingLot = 0
        }

        for (i <- 1 to numberOfParkingLot) {
            availableSlots.add(i)
        }

        if (numberOfParkingLot > 0) {
            println(s"Created a parking lot with $numberOfParkingLot slots")
        } else {
            println("Sorry, parking lot is not created")
        }
    }

    /**
     * Add vehicle park
     *
     * @param regNumber - Registration number
     * @param color     - Color of vehicle
     */
    override def park(regNumber: String, color: String): Unit = {
        numberOfParkingLot match {
            case 0 =>
                System.out.println("Sorry, parking lot is not created")
            case x if mapCar.size() == x =>
                System.out.println("Sorry, parking lot is full")
            case _ =>
                val slot = availableSlots.get(0).toString
                val car = Car(regNumber, color)

                mapCar.put(slot, car)
                mapRegColor.put(regNumber, slot)

                if (mapRegsColor.containsKey(color)) {
                    val regs = mapRegsColor.get(color)
                    mapRegsColor.remove(color)
                    regs.add(regNumber)
                    mapRegsColor.put(color, regs)
                } else {
                    val regs = new ArrayList[String]()
                    regs.add(regNumber)
                    mapRegsColor.put(color, regs)
                }

                System.out.println(s"Allocated slot number: $slot")
                availableSlots.remove(0)
        }
    }

    /**
     * Leave the parking lot
     *
     * @param slotNumber - Parking slot number
     */
    override def leave(slotNumber: String): Unit = {
        numberOfParkingLot match {
            case 0 =>
                System.out.println("Sorry, parking lot is not created")
            case x if mapCar.size() == x =>
                Some(mapCar.get(slotNumber)).map { data =>
                    mapCar.remove(slotNumber)
                    mapRegColor.remove(data.registrationNumber)

                    val regs = mapRegsColor.get(data.color)
                    if (regs.contains(data.registrationNumber)) {
                        regs.remove(data.registrationNumber)
                    }

                    availableSlots.add(slotNumber.toInt)

                    System.out.println(s"Slot number $slotNumber is free")
                }.getOrElse {
                    System.out.println(s"Slot number $slotNumber is already empty")
                }
            case _ =>
                System.out.println("Parking lot is empty")
        }
    }

    /**
     * Show information from parking lot
     */
    override def status(): Unit = {
        numberOfParkingLot match {
            case 0 => System.out.println("Sorry, parking lot is not created")
            case _  =>
                if (mapCar.size() > 0) {
                    System.out.println("Slot No.\tRegistration No\tColour")
                    for (i <- 1 to numberOfParkingLot) {
                        val keyStr = i.toString
                        if (mapCar.containsKey(keyStr)) {
                            val car = mapCar.get(keyStr)
                            System.out.println(s"$i\t${car.registrationNumber}\t${car.color}")
                        }
                    }
                } else {
                    System.out.println("Parking lot is empty")
                }
        }
    }

    /**
     * Show vehicle for vehicle with color
     *
     * @param color - Color of vehicle
     */
    override def registrationNumbersForCarsWithColor(color: String): Unit = {
        numberOfParkingLot match {
            case 0 => System.out.println("Sorry, parking lot is not created")
            case _ =>
                if (mapRegsColor.containsKey(color)) {
                    val regs = mapRegsColor.get(color)
                    for (i <- 0 until regs.size()) {
                        if (i == regs.size() - 1) {
                            System.out.println(regs.get(i))
                        } else {
                            System.out.println(s"${regs.get(i)},")
                        }
                    }
                } else {
                    System.out.println("Not found")
                }
        }
    }

    /**
     * Show slot numbers for vehicle with color
     *
     * @param color - Color of vehicle
     */
    override def slotNumbersForCarsWithColor(color: String): Unit = {
        numberOfParkingLot match {
            case 0 => System.out.println("Sorry, parking lot is not created")
            case _ =>
                if (mapRegsColor.containsKey(color)) {
                    val regs = mapRegsColor.get(color)
                    var slots = Seq.empty[Int]

                    for (i <- 0 until regs.size()) {
                        slots = slots :+ mapRegColor.get(regs.get(i)).toInt
                    }

                    for (j <- slots.indices) {
                        if (j == slots.length - 1) {
                            System.out.println(slots.apply(j))
                        } else {
                            System.out.println(s"${slots.apply(j)},")
                        }
                    }
                } else {
                    println("Not found")
                }
        }
    }

    /**
     * Show slot number for registration number
     *
     * @param regNumber - Registration number
     */
    override def slotNumberForRegistrationNumber(regNumber: String): Unit = {
        numberOfParkingLot match {
            case 0 => System.out.println("Sorry, parking lot is not created")
            case _ =>
                if (mapRegColor.containsKey(regNumber)) {
                    System.out.println(mapRegColor.get(regNumber))
                } else {
                    System.out.println("Not found")
                }
        }
    }
}
