package com.gojek.parkinglot.model

abstract class Vehicle(registrationNumber: String, color: String)
