package com.gojek.parkinglot.model

/**
 * Model class for Car
 * @param registrationNumber - Car registration number
 * @param color - Color of car
 */
case class Car(registrationNumber: String, color: String) extends Vehicle(registrationNumber, color)
