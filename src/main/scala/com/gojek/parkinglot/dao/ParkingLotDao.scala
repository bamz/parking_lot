package com.gojek.parkinglot.dao

trait ParkingLotDao {

    /**
     * Create parking lot
     * @param lotCount - parking lot count
     */
    protected def createParkingLot(lotCount: String)

    /**
     * Add vehicle park
     * @param regNumber - Registration number
     * @param color - Color of vehicle
     */
    protected def park(regNumber: String, color: String)

    /**
     * Leave the parking lot
     * @param slotNumber - Parking slot number
     */
    protected def leave(slotNumber: String)

    /**
     * Show information from parking lot
     */
    protected def status()

    /**
     * Show vehicle for vehicle with color
     * @param color - Color of vehicle
     */
    protected def registrationNumbersForCarsWithColor(color: String)

    /**
     * Show slot numbers for vehicle with color
     * @param color - Color of vehicle
     */
    protected def slotNumbersForCarsWithColor(color: String)

    /**
     * Show slot number for registration number
     * @param regNumber - Registration number
     */
    protected def slotNumberForRegistrationNumber(regNumber: String)

}
