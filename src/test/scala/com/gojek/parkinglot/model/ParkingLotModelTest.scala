package com.gojek.parkinglot.model

import java.io.{ByteArrayOutputStream, PrintStream}
import org.specs2.mutable.Specification
import org.specs2.specification.AfterAll


class ParkingLotModelTest extends Specification with AfterAll {

    override def is = {
        sequential ^
            "Parking Lot Model should" ^
            p ^
                "Create parking lot" ! createParkingLot ^
                "Park vehicle" ! park ^
                "Leave parking lot" ! leave ^
                "Show status" ! status ^
                "Registration Numbers for Cars with Color" ! registrationNumbersForCarsWithColor ^
                "Slot Numbers for Cars with Color" ! slotNumbersForCarsWithColor ^
                "Slot Number for Registration Number" ! slotNumberForRegistrationNumber ^
            end
    }

    def createParkingLot = {
        val parkingLot = new ParkingLotModel()
        parkingLot.createParkingLot("a")
        0 mustEqual parkingLot.numberOfParkingLot

        parkingLot.createParkingLot("6")
        6 mustEqual parkingLot.numberOfParkingLot
        6 mustEqual parkingLot.availableSlots.size()
    }

    def park = {
        val outContent: ByteArrayOutputStream = new ByteArrayOutputStream()
        System.setOut(new PrintStream(outContent))

        val parkingLot = new ParkingLotModel()
        parkingLot.createParkingLot("3")
        parkingLot.park("KA-01-HH-1234", "White")
        parkingLot.park("KA-01-HH-9999", "White")
        parkingLot.park("KA-01-BB-0001", "Black")
        parkingLot.park("KA-01-HH-7777", "Red")
        """
          |Allocated slot number: 1
          |Allocated slot number: 2
          |Allocated slot number: 3
          |Sorry, parking lot is full
          |""".stripMargin.trim mustEqual outContent.toString().trim()
    }

    def leave = {
        val outContent: ByteArrayOutputStream = new ByteArrayOutputStream()
        System.setOut(new PrintStream(outContent))

        val parkingLot = new ParkingLotModel()
        parkingLot.createParkingLot("2")
        parkingLot.park("KA-01-HH-1234", "White")
        parkingLot.park("KA-01-HH-9999", "White")
        parkingLot.leave("1")
        """
          |Allocated slot number: 1
          |Allocated slot number: 2
          |Slot number 1 is free
          |""".stripMargin.trim mustEqual outContent.toString().trim()
    }

    def status = {
        val outContent: ByteArrayOutputStream = new ByteArrayOutputStream()
        System.setOut(new PrintStream(outContent))

        val parkingLot = new ParkingLotModel()
        parkingLot.createParkingLot("3")
        parkingLot.park("KA-01-HH-1234", "White")
        parkingLot.park("KA-01-HH-9999", "White")
        parkingLot.park("KA-01-BB-0001", "Black")
        parkingLot.leave("2")
        parkingLot.status()
        """
          |Allocated slot number: 1
          |Allocated slot number: 2
          |Allocated slot number: 3
          |Slot number 2 is free
          |Slot No.	Registration No	Colour
          |1	KA-01-HH-1234	White
          |3	KA-01-BB-0001	Black
          |""".stripMargin.trim mustEqual outContent.toString().trim()
    }

    def registrationNumbersForCarsWithColor = {
        val outContent: ByteArrayOutputStream = new ByteArrayOutputStream()
        System.setOut(new PrintStream(outContent))

        val parkingLot = new ParkingLotModel()
        parkingLot.createParkingLot("3")
        parkingLot.park("KA-01-HH-1234", "White")
        parkingLot.park("KA-01-HH-9999", "White")
        parkingLot.park("KA-01-BB-0001", "Black")
        parkingLot.leave("2")
        parkingLot.status()
        parkingLot.registrationNumbersForCarsWithColor("White")
        parkingLot.registrationNumbersForCarsWithColor("Red")
        """
          |Allocated slot number: 1
          |Allocated slot number: 2
          |Allocated slot number: 3
          |Slot number 2 is free
          |Slot No.	Registration No	Colour
          |1	KA-01-HH-1234	White
          |3	KA-01-BB-0001	Black
          |KA-01-HH-1234
          |Not found
          |""".stripMargin.trim mustEqual outContent.toString().trim()
    }

    def slotNumbersForCarsWithColor = {
        val outContent: ByteArrayOutputStream = new ByteArrayOutputStream()
        System.setOut(new PrintStream(outContent))

        val parkingLot = new ParkingLotModel()
        parkingLot.createParkingLot("3")
        parkingLot.park("KA-01-HH-1234", "White")
        parkingLot.park("KA-01-HH-9999", "White")
        parkingLot.park("KA-01-BB-0001", "Black")
        parkingLot.leave("1")
        parkingLot.status()
        parkingLot.registrationNumbersForCarsWithColor("White")
        parkingLot.registrationNumbersForCarsWithColor("Red")
        parkingLot.slotNumbersForCarsWithColor("White")
        """
          |Allocated slot number: 1
          |Allocated slot number: 2
          |Allocated slot number: 3
          |Slot number 1 is free
          |Slot No.	Registration No	Colour
          |2	KA-01-HH-9999	White
          |3	KA-01-BB-0001	Black
          |KA-01-HH-9999
          |Not found
          |2
          |""".stripMargin.trim mustEqual outContent.toString().trim()
    }

    def slotNumberForRegistrationNumber = {
        val outContent: ByteArrayOutputStream = new ByteArrayOutputStream()
        System.setOut(new PrintStream(outContent))

        val parkingLot = new ParkingLotModel()
        parkingLot.createParkingLot("4")
        parkingLot.park("KA-01-HH-1234", "White")
        parkingLot.park("KA-01-HH-9999", "White")
        parkingLot.park("KA-01-BB-0001", "Black")
        parkingLot.park("KA-01-HH-7777", "Red")
        parkingLot.status()
        parkingLot.registrationNumbersForCarsWithColor("Red")
        parkingLot.slotNumbersForCarsWithColor("Red")
        parkingLot.slotNumberForRegistrationNumber("KA-01-BB-0001")
        """
          |Allocated slot number: 1
          |Allocated slot number: 2
          |Allocated slot number: 3
          |Allocated slot number: 4
          |Slot No.	Registration No	Colour
          |1	KA-01-HH-1234	White
          |2	KA-01-HH-9999	White
          |3	KA-01-BB-0001	Black
          |4	KA-01-HH-7777	Red
          |KA-01-HH-7777
          |4
          |3
          |""".stripMargin.trim mustEqual outContent.toString().trim()
    }

    override def afterAll(): Unit = {
        System.setOut(null)
    }
}
