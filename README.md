# ParkingLot 1.4.2

Automated ticketing system that allows a customers to use parking lot without human intervention.

Requirements:
- sbt 1.3.8

    MacOS
    ```
    $ brew install sbt
    ```
  
    Debian/Ubuntu
    ```
    $ sudo apt install sbt
    ```
- scala 2.13.1
- java jdk8 or later

### Run the application

First, run setup script:
```
$ ./bin/setup
```

Script to run the program:
```
$ ./bin/parking_lot
```
